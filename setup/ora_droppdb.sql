--
--
set echo on

-- EWID
connect sys/manager@//tst81:31521 as sysdba;


alter pluggable database ewid close;
drop pluggable database ewid including  datafiles;

alter pluggable database ewii close;
drop pluggable database ewii including  datafiles;

alter pluggable database ewia close;
drop pluggable database ewia including  datafiles;

alter pluggable database ewip close;
drop pluggable database ewip including  datafiles;

exit
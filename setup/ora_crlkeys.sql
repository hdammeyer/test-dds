--
--
set echo on
--
connect system/manager@//tst81:31521
--
ALTER SESSION SET CONTAINER=ewid;
create user L005711 identified by baloise01;
@@ora_crrole.sql
grant connect,resource to L005711;
grant role_edwh_adm to L005711;
--
--
ALTER SESSION SET CONTAINER=ewii;
create user L005712 identified by baloise01;
@@ora_crrole.sql
grant connect,resource to L005712;
grant role_edwh_adm to L005712;
--
--
ALTER SESSION SET CONTAINER=ewia;
create user L005713 identified by baloise01;
@@ora_crrole.sql
grant connect,resource to L005713;
grant role_edwh_adm to L005713;
--
--
ALTER SESSION SET CONTAINER=ewip;
create user L005714 identified by baloise01;
@@ora_crrole.sql
grant connect,resource to L005714;
grant role_edwh_adm to L005714;
--
exit
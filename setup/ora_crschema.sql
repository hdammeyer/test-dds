--
--
set echo on
--
connect system/manager@//tst81:31521
--
ALTER SESSION SET CONTAINER=ewid;
create tablespace users ;
create user edwh_uk_risk_d identified by manager default tablespace users;
alter user edwh_uk_risk_d quota unlimited on users;
grant create session, create table, create sequence, create view to edwh_uk_risk_d;
create user edwh_uk_care_d identified by manager default tablespace users;
alter user edwh_uk_care_d quota unlimited on users;
grant create session, create table, create sequence, create view to edwh_uk_care_d;
--
--
ALTER SESSION SET CONTAINER=ewii;
create tablespace users ;
create user edwh_uk_risk_i identified by manager default tablespace users;
alter user edwh_uk_risk_i quota unlimited on users;
grant create session, create table, create sequence, create view to edwh_uk_risk_i;
create user edwh_uk_care_i identified by manager default tablespace users;
alter user edwh_uk_care_i quota unlimited on users;
grant create session, create table, create sequence, create view to edwh_uk_care_i;
--
--
ALTER SESSION SET CONTAINER=ewia;
create tablespace users ;
create user edwh_uk_risk_a identified by manager default tablespace users;
alter user edwh_uk_risk_a quota unlimited on users;
grant create session, create table, create sequence, create view to edwh_uk_risk_a;
create user edwh_uk_care_a identified by manager default tablespace users;
alter user edwh_uk_care_a quota unlimited on users;
grant create session, create table, create sequence, create view to edwh_uk_care_a;
--
--
ALTER SESSION SET CONTAINER=ewip;
create tablespace users ;
create user edwh_uk_risk_p identified by manager default tablespace users;
alter user edwh_uk_risk_p quota unlimited on users;
grant create session, create table, create sequence, create view to edwh_uk_risk_p;
create user edwh_uk_care_p identified by manager default tablespace users;
alter user edwh_uk_care_p quota unlimited on users;
grant create session, create table, create sequence, create view to edwh_uk_care_p;
--
exit
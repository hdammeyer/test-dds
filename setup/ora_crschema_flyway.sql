--
--
set echo on
--
connect system/manager@//tst81:31521
--
ALTER SESSION SET CONTAINER=ewid;
create tablespace flyway_admin ;
create user flyway_admin  identified by baloise01 default tablespace flyway_admin;
alter user  flyway_admin  quota unlimited on flyway_admin;
grant create session, create table, create sequence, create view to  flyway_admin ;
--
--
ALTER SESSION SET CONTAINER=ewii;
create tablespace flyway_admin ;
create user  flyway_admin  identified by baloise01 default tablespace flyway_admin;
alter user  flyway_admin  quota unlimited on flyway_admin;
grant create session, create table, create sequence, create view to  flyway_admin ;
--
--
ALTER SESSION SET CONTAINER=ewia;
create tablespace flyway_admin ;
create user  flyway_admin  identified by baloise01 default tablespace flyway_admin;
alter user  flyway_admin  quota unlimited on flyway_admin;
grant create session, create table, create sequence, create view to  flyway_admin ;
--
--
ALTER SESSION SET CONTAINER=ewip;
create tablespace flyway_admin ;
create user  flyway_admin  identified by baloise01 default tablespace flyway_admin;
alter user  flyway_admin  quota unlimited on flyway_admin;
grant create session, create table, create sequence, create view to  flyway_admin ;
--
exit
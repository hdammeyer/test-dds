--
--
set echo on

-- EWID
connect sys/manager@//tst81:31521 as sysdba;

create pluggable database ewid admin user ewid_adm identified by manager;
alter pluggable database ewid open;
alter session set container=ewid;
BEGIN 
  dbms_service.create_service('ewid.dammeyer.local','ewid.dammeyer.local');
END;
/

-- EWII
connect sys/manager@//tst81:31521 as sysdba;

create pluggable database ewii admin user ewii_adm identified by manager;
alter pluggable database ewii open;
alter session set container=ewii;
BEGIN 
  dbms_service.create_service('ewii.dammeyer.local','ewii.dammeyer.local');
END;
/

-- EWIA
connect sys/manager@//tst81:31521 as sysdba;

create pluggable database ewia admin user ewia_adm identified by manager;
alter pluggable database ewia open;
alter session set container=ewia;
BEGIN 
  dbms_service.create_service('ewia.dammeyer.local','ewia.dammeyer.local');
END;
/

-- EWIP
connect sys/manager@//tst81:31521 as sysdba;

create pluggable database ewip admin user ewip_adm identified by manager;
alter pluggable database ewip open;
alter session set container=ewip;
BEGIN 
  dbms_service.create_service('ewip.dammeyer.local','ewip.dammeyer.local');
END;
/

alter system register;

exit

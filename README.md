# test dds

Mit diesem Repo wird die per Flyway automatisiert Ausführung von SQL Scripts getestet.

Im Bitbucket Repo

```bash
origin  git@bitbucket.org:hdammeyer/test-dds.git (fetch)
origin  git@bitbucket.org:hdammeyer/test-dds.git (push)
```

ist ein Webhook konfiguriert über den ein Jenkinsserver getriggert wird.

URL:

```bash
http://l9ai4e660v9fs0bx.myfritz.net:18081/git/notifyCommit?url=ssh://hdammeyer@bitbucket.org/hdammeyer/test-dds.git
````

Der Jenkinsserver wird per Docker Container bereitgestellt. Docker wiederum läuft auf einer VM auf einem MacBook Pro (MBP).

Damit das funktioniert, muss die Fritzbox unter [MyFritz!](https://www.myfritz.net) registiert werden. In der Fritzbox wird der Port 18081 freigegeben und mit dem Port 18081 des MBP verknüpft. Der Port 8081 Centos VM wird in Virtualbox per Portforwarding mit dem Socket MBP:18081 verbunden.

test test
test test
test test
test test
test test
test test

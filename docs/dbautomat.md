---
filename: dbautomat.md
---

# Flyway

DB Changes sollen automatisiert in einer Datenbank appliziert werden. Im Vergleich zur Ausführung durch Dritte (HCL, GroupIT), ist eine bessere und direkte Kontrolle möglich. Automatisierte DB Changes sind notwendig, wenn CI/CD Prozesse umgesetzt werden sollen.

Für die automatisierte Ausführung der SQL Statements wurde Flyway gewählt. Es folgt ein kurzer Überblick zu den Möglichkeiten und Einschränkungen von Flyway. So wird ein besseres Verständnis geschaffen, das einen effizienteren Einsatz ermöglicht.

## Wozu benötigen wir Flyway?

Um SQL Statements/Scipte in einer Datenbank auszuführen wird ein Client Tool benötigt. Für den manuellen, interaktiven Einsatz sind bei der Baloise diverse Produkte in Verwendung.

* Toad for Oracle, for db2, for DataPoint
* SQL*Plus
* SQL*Developer
* dbeaver

Jedes dieser Produkte bietet eine grosse Vielfalt von Optionen, welche jedoch bei einer automatisierten Ausführung nicht benötigt werden. Zum Beispiel das Formatieren von SQL Statements. Ausserdem handelt es sich um Tools mit einem GUI und sind so nicht die erste Wahl um SQLs automatisiert auszuführen.

Im Wesentlichen wird benötigt

* An-/Abmelden an/von einer Datenbank
* SQL zur Ausführung an  DB senden
* Ergebnis der Ausführung empfangen und verarbeiten
* Integration in CI/CD

Flyway bietet diese Möglichkeiten und mehr.

## Was ist Flyway

> Flyway is an open-source database migration tool.

Flyway ermöglicht Versionskontrolle für Datenbanken (bzw DB Schema*ta*). Durch Versionierung werden DB Migrationen kontrollierbar und nachvollziehbar.

* **Versionskontrolle**
* unterstützt verschiedene DB Produkte (db2, Oracle, PostgreSQL, ...)
* kontrollierte,nachvollziehbare Migrationsprozesse
* CI/CD Integration
* SQL ausführen und protokollieren

Neben einem Java API und Plugins für Maven und Gradle, bietet Flyway ein CLI. Dieser Command Line Client kommt bei der Baloise für die automatisierte Ausführung von SQL Scripten zum Einsatz.

Es gibt sieben Anweisungen

* migrate
* clean ('evil')
* info
* validate
* undo (flyway teams)
* baseline
* repair

Neben der Community Edition (CE) gibt es noch eine [Flyway Teams Edition (TE)](https://flywaydb.org/download), die über zusätzliche Funktionen verfügt. Für die automatisierte Ausführung von SQL bei der Baloise ist die Flyway Community Edition vorgesehen.

Die aufgeführten Anweisungen dienen dem Zweck versionierte Migrationen von DB Schemata durchzuführen. Beschränken wir uns auf die reine Auführung von SQL reicht die Verwendung von ```migrate``` . Weiter könnten ```info``` und ```validate``` sich als nützlich erweisen.

## Was beim Einsatz von Flyway zu beachten?

Wie erwähnt ist das Ziel von Flyway die versionierte Migrationen von Datenbank **Schemata**.

Damit dieses Ziel erreicht wird, müssen für die Ausführung von SQL diverse Anforderungen erfüllt und Prinzipien berücksichtigt werden. Zum Beispiel wird die Ausführung von SQLs in einer History Tabelle protokolliert. So wird verhindert, dass SQL Files nicht wiederholt prozessiert wird. Es wird auch der Hashwert eines verarbeitetn SQL Files protokolliert. So kann gewährleistet werden, dass SQL Files nicht nachträglich verändert werden.

* Namenskonvention für SQL Files
* Umgebungsspezifischen Variablen\
wie z.B. Schemaname, db name, location der SQLs, username/password usw
* Speicherorte der SQL Files
* History Table
* InScope vs OutOfScope

## Testumgebung Heiko

Neben vier Datenbanken werden lokal auch Jenkins und Flyway per Docker Container bereitgestellt. Die Container kommunizieren via dem Docker Netz oracle_network miteinander.

Das zentrale Git Repository liegt extern unter [bitbucket.org](https://bitbucket.org/hdammeyer/test-dds).

Für die Bereitstellung der SQL Scripts wurde das Git Repository lokal geclont.

![bild](./img/dbautomat02.svg)

### Datenbanken

Es werden Schemata zweier Datenbanken automatisiert.

* SAS Enterprise Dataware House: db2 xchedwh
* Shared Data Model: oracle ewix

| DB | L-Key| IP | Service Name
|--|--|--|--|
| ewid | L005711 | 172.17.0.2 | ewii |
| ewii | L005712 | 172.17.0.2 | ewii |
| ewia | L005713 | 172.17.0.2 | ewia |
| ewip | L005714 | 172.17.02 | ewip |
| dchedwh | L004711 | 172.17.0.5 | - |
| ichedwh | L004712 | 172.17.0.3 | - |
| achedwh | L004713 | 172.17.0.4 | - |
| pchedwh | L004714 | 172.17.0.6 | - |

Die IP Adressen kommen vom Docker netzwerk oracle_network. Die Oracle DBs werden als PDBs bereitsgestellt. Daher sind sie über eine IP Adresse zu erreichen und es muss zusaetzlich der Service Name mitgegeben werden.

## Docker network

Damit zwischen den Containern Verbindungen aufgebaut werden können, wird ein eigenes Netzwerk definiert.

```bash
docker network ls
docker network oracle_network
```

Container können nachträglich mit dem Netzwerk verbunden werden.

```bash
docker network connect oracle_network orcl
docker network connect oracle_network myjenkins2
```

## Jenkins docker

Jenkins kann lokal als Container bereitgestellt werden, ein entsprechendes Image its auf [Github/Jenkins](https://hub.docker.com/r/jenkins/jenkins) verfügbar.

```bash
 docker run --name myjenkins2 -u root -d --network=oracle_network   -p 8081:8080 -p 50001:50000 -e JENKINS_HOME='/root/jenkins-data' -v /root/jenkins-data:/root/jenkins-data -v $(which docker):/usr/bin/docker -v /var/run/docker.sock:/var/run/docker.sock -v "$HOME":/home jenkins/jenkinsjenkins:jdk11
```

## Flyway Docker

```bash
docker run --network=oracle_network --rm flyway/flyway -v
```

## db2 docker

Ein COntainer Image für eine DB2 Datenbank wird auf [Github/db2](https://hub.docker.com/r/ibmcom/db2) bereitgestelt.

Beim Erstellen des Containers wird gleichzeitig die DB mit erstellt:

* dchedwh
* ichedwh
* achedwh
* pchedwh

* dchedwh

```bash
docker run -itd --name mydb2d --privileged=true -p 60001:50000 -e LICENSE=accept \
 -e DB2INST1_PASSWORD=manager \
 -e DBNAME=dchedwh \
 -v ~/docker/db2/dchedwh:/database \
 ibmcom/db2
```

* ichedwh

```bash
docker run -itd --name mydb2d --privileged=true -p 60002:50000 -e LICENSE=accept \
 -e DB2INST1_PASSWORD=manager \
 -e DBNAME=ichedwh \
 -v ~/docker/db2/ichedwh:/database \
 ibmcom/db2
```

* achedwh

```bash
docker run -itd --name mydb2d --privileged=true -p 60003:50000 -e LICENSE=accept \
 -e DB2INST1_PASSWORD=manager \
 -e DBNAME=achedwh \
 -v ~/docker/db2/achedwh:/database \
 ibmcom/db2
```

* pchedwh

```bash
docker run -itd --name mydb2p --privileged=true -p 60004:50000 -e LICENSE=accept \
 -e DB2INST1_PASSWORD=manager \
 -e DBNAME=pchedwh \
 -v ~/docker/db2/pchedwh:/database \
 ibmcom/db2
```

der db2 l-key account wird jeweils im docker container angelegt. Damit der Account über die notwendigen Berechtigungen verfügt, bekommt er die Gruppe db2iadm1 zugewiesen.

```bash
docker exec -it mydb2d /bin/bash
useradd -g db2iadm1 l004711
passwd l004711
```

Der L-Key benötigt ausserdem Rechte in der DB:

```bash
export DB2INSTANCE=db2inst1
docker exec -ti mydb2d bash -c "su - ${DB2INSTANCE}"
source sqllib/db2profile
db2 "connect to dchedwh"
db2 "grant dbadm on database to user l004711"
```

Sind Datenbanken und L-Keys angelegt können in den DBs die Schemata definiert werden.

```sql
-- 
CREATE SCHEMA CH_ADMIN;
CREATE SCHEMA CH_EXTRACTION;
CREATE SCHEMA CH_TRANSFORMATION;
CREATE SCHEMA CH_STANDARD_DATAMODEL;

CREATE SCHEMA FLYWAY_ADMIN ;
```

## oracle docker

Für Oracle können zum Einen die von Oracle bereitgestellten Images verwendet werden, [Github/oracle](https://github.com/oracle/docker-images). Dabei ist jedoch zu beachten, dass die entsprechend notwendigen Lizenzen benötigt werden.

Ein Image für Oracle eXpress Edition wird von Gerald Venzl bereitgestellt: [Github/gvenzl](https://hub.docker.com/r/gvenzl/oracle-xe).

```bash
docker run -d -p 31521:1521 --network=oracle_network --name=orcl -e ORACLE_PASSWORD=manager gvenzl/oracle-xe
```

Nachdem die Oracle 21c ContainerDB erstellt ist, werden die Pluggable Databases erstellt.

* EWID
* EWII
* EWIA
* EWIP

```sql
setup/ora_crpdb.sql
```

Anschliessend werden in den PDBS Schemata und User erstellt.

```sql
setup/ora_crschema.sql
setup/ora_crschema_flyway.sql
setup/ora_crlkeys.sql
```
